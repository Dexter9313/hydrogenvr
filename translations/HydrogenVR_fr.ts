<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AbstractMainWin</name>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="115"/>
        <source>Save Screenshot</source>
        <translation>Sauver une Capture d&apos;Écran</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="117"/>
        <source>Images (*.png *.xpm *.jpg)</source>
        <translation>Images (*.png *.xpm *.jpg)</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="477"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="478"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="480"/>
        <source>HydrogenVR</source>
        <translation>HydrogenVR</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="481"/>
        <source>Explore Shaders...</source>
        <translation>Explorer les Shaders...</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="486"/>
        <source>Explore Textures...</source>
        <translation>Explorer les Textures...</translation>
    </message>
    <message>
        <location filename="../src/AbstractMainWin.cpp" line="492"/>
        <source>Log Profiling Timings</source>
        <translation>Loguer les Temps de Profilage</translation>
    </message>
</context>
<context>
    <name>BaseInputManager</name>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="23"/>
        <source>Toggle Debug Camera</source>
        <translation>Activer/Désactiver la Caméra de Debug</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="24"/>
        <source>Toggle Wireframe Mode</source>
        <translation>Activer/Désactiver le Mode Fil de Fer</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="26"/>
        <source>Toggle Diagnostics Mode</source>
        <translation>Activer/Désactiver le Mode Diagnostique</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="28"/>
        <source>Reload Python Engine</source>
        <translation>Recharger le Moteur Python</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="30"/>
        <source>Toggle Python Console</source>
        <translation>Activer/Désactiver la Console Python</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="32"/>
        <source>Take Screenshot</source>
        <translation>Prendre une Capture d&apos;Écran</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="33"/>
        <source>Toggle Virtual Reality</source>
        <translation>Activer/Désactiver la Réalité Virtuelle</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="39"/>
        <source>Toggle Fullscreen</source>
        <translation>Activer/Désactiver le Plein Écran</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="40"/>
        <source>Toggle Automatic Exposure</source>
        <translation>Activer/Désactiver l&apos;Exposition Automatique</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="42"/>
        <source>Increase Exposure</source>
        <translation>Augmenter l&apos;Exposition</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="43"/>
        <source>Decrease Exposure</source>
        <translation>Diminuer l&apos;Exposition</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="45"/>
        <source>Increase Dynamic Range</source>
        <translation>Augmenter la Gamme Dynamique</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="48"/>
        <source>Decrease Dynamic Range</source>
        <translation>Diminuer la Gamme Dynamique</translation>
    </message>
    <message>
        <location filename="../src/BaseInputManager.cpp" line="49"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>BaseLauncher</name>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="23"/>
        <source> Launcher</source>
        <translation> Lanceur</translation>
    </message>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="53"/>
        <source>LAUNCH</source>
        <translation>LANCER</translation>
    </message>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="59"/>
        <source>RESET TO DEFAULT</source>
        <translation>RÉINITIALISER</translation>
    </message>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="64"/>
        <source>QUIT</source>
        <translation>QUITTER</translation>
    </message>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="76"/>
        <source>Resetting to default</source>
        <translation>Réinitialisation</translation>
    </message>
    <message>
        <location filename="../src/BaseLauncher.cpp" line="77"/>
        <source>Are you sure you want to reset your settings to default ?</source>
        <translation>Êtes-vous sûr que vous voulez réinizialiser les paramètres ?</translation>
    </message>
</context>
<context>
    <name>DemoDialog</name>
    <message>
        <location filename="../example/src/DemoDialog.cpp" line="24"/>
        <source>Demo 3D Dialog</source>
        <translation>3D Dialog Démo</translation>
    </message>
    <message>
        <location filename="../example/src/DemoDialog.cpp" line="38"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>ExampleSettings</name>
    <message>
        <location filename="../example/src/ExampleSettings.cpp" line="24"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../example/src/ExampleSettings.cpp" line="25"/>
        <source>Toggle mouse view</source>
        <translation>Activer/Désactiver la Vue par Souris</translation>
    </message>
</context>
<context>
    <name>InputManager</name>
    <message>
        <location filename="../example/include/InputManager.hpp" line="29"/>
        <source>Barrel Power Up</source>
        <translation>Augmenter la Puissance de Barillet</translation>
    </message>
    <message>
        <location filename="../example/include/InputManager.hpp" line="30"/>
        <source>Barrel Power Down</source>
        <translation>Diminuer la Puissance de Barillet</translation>
    </message>
    <message>
        <location filename="../example/include/InputManager.hpp" line="31"/>
        <source>Toggle VR origin</source>
        <translation>Basculer l&apos;Origine RV</translation>
    </message>
</context>
<context>
    <name>MainWin</name>
    <message>
        <source>Hello World !
Let&apos;s draw some text !</source>
        <translation type="vanished">Hello World !
Affichons du texte !</translation>
    </message>
    <message>
        <location filename="../example/src/MainWin.cpp" line="302"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../example/src/MainWin.cpp" line="303"/>
        <source>Demo Dialog</source>
        <translation>Dialogue de Démo</translation>
    </message>
    <message>
        <location filename="../example/src/MainWin.cpp" line="317"/>
        <source>Hello World !
</source>
        <translation>Hello World !
</translation>
    </message>
</context>
<context>
    <name>RenderingWindow</name>
    <message>
        <location filename="../src/RenderingWindow.cpp" line="31"/>
        <source> - Subwindow </source>
        <translation> - Sous-Fenêtre </translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="58"/>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <source>Window Width</source>
        <translation type="vanished">Largeur de la Fenêtre</translation>
    </message>
    <message>
        <source>Window Height</source>
        <translation type="vanished">Hauteur de la Fenêtre</translation>
    </message>
    <message>
        <source>Window Fullscreen</source>
        <translation type="vanished">Plein Écran</translation>
    </message>
    <message>
        <source>High Dynamic Range</source>
        <translation type="vanished">Haute Gamme Dynamique</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="60"/>
        <source>Enable VSYNC</source>
        <translation>Activer la VSYNC</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="62"/>
        <source>Force Rendering Resolution</source>
        <translation>Forcer la Résolution de Rendu</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="63"/>
        <source>Forced Rendering Width</source>
        <translation>Largeur de Rendu Forcée</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="64"/>
        <source>Forced Rendering Height</source>
        <translation>Hauteur de Rendu Forcée</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="68"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="68"/>
        <source>Panorama 360</source>
        <translation>Panorama 360</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="69"/>
        <source>VR 180 Left</source>
        <translation>VR 180 Gauche</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="69"/>
        <source>VR 180 Right</source>
        <translation>VR 180 Droite</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="70"/>
        <source>VR 180</source>
        <translation>VR 180</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="70"/>
        <source>Domemaster 180</source>
        <translation>Domemaster 180</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="71"/>
        <source>Projection</source>
        <translation>Projection</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="73"/>
        <source>Start with video mode enabled</source>
        <translation>Démarrer avec le mode vidéo activé</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="75"/>
        <source>Quit after this number of frames rendered</source>
        <translation>Quitter après ce nombre de frames rendues</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="76"/>
        <source>Video target FPS</source>
        <translation>FPS cible de la Vidéo</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="81"/>
        <source>Video Frames Output Directory</source>
        <translation>Dossier Destination des Frames de la Vidéo</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="83"/>
        <source>Graphics</source>
        <translation>Graphismes</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="84"/>
        <source>Anti-aliasing</source>
        <translation>Anti-aliasing</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="85"/>
        <source>Shadows Quality</source>
        <translation>Qualité des Ombres</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="86"/>
        <source>Shadow Smoothing Quality</source>
        <translation>Qualité de l&apos;Adoucissement des Ombres</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="87"/>
        <source>Enable Dithering</source>
        <translation>Atténuer l&apos;Effet de Bande</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="88"/>
        <source>Bloom</source>
        <translation>Bloom</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="89"/>
        <source>Vertical field of view (0=auto)</source>
        <translation>Champs de vision vertical (0=auto)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="91"/>
        <source>Horizontal field of view (0=auto)</source>
        <translation>Champs de vision horizontal (0=auto)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="95"/>
        <source>Controls</source>
        <translation>Contrôles</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="96"/>
        <location filename="../src/SettingsWidget.cpp" line="249"/>
        <source>ENGINE</source>
        <translation>MOTEUR</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="99"/>
        <location filename="../src/SettingsWidget.cpp" line="115"/>
        <source>None</source>
        <translation>Aucune</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="108"/>
        <source>Gamepad</source>
        <translation>Manette</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="110"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="154"/>
        <source>Virtual Reality</source>
        <translation>Réalité Virtuelle</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="155"/>
        <source>Enable VR</source>
        <translation>Activer la RV</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="157"/>
        <source>OpenVR</source>
        <translation>OpenVR</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="157"/>
        <source>Stereo Beamer</source>
        <translation>Projecteur Stéréo</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="158"/>
        <source>Handler</source>
        <translation>Module</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="161"/>
        <source>Force 2D render on screen
(will decrease performance !)</source>
        <translation>Forcer le rendu 2D sur l&apos;écran
(va impacter négativement les performances !)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="163"/>
        <source>Stereo multiplier (if applicable)</source>
        <translation>Multiplicateur stéréo (si applicable)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="167"/>
        <source>Screen height (in meters)</source>
        <translation>Hauteur d&apos;écran (en mètres)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="169"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="170"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="171"/>
        <source>Client ID</source>
        <translation>ID Client</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="172"/>
        <source>IP address of server (if client)</source>
        <translation>Adresse IP du serveur (si client)</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="173"/>
        <source>UDP port</source>
        <translation>Port UDP</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="174"/>
        <source>TCP port</source>
        <translation>Port TCP</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="189"/>
        <source>Scripting</source>
        <translation>Scripting</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="191"/>
        <source>Scripts Root Directory</source>
        <translation>Dossier Racine des Scripts</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="192"/>
        <source>Custom Root Directory</source>
        <translation>Dossier Racine Personnalisé</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="194"/>
        <source>Debug Camera</source>
        <translation>Caméra de Debug</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="195"/>
        <source>Enable Debug Camera</source>
        <translation>Activer la Caméra de Debug</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="196"/>
        <source>Follow HMD Movement</source>
        <translation>Suivre les Mouvements du Casque RV</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="197"/>
        <source>Show Debug In HMD</source>
        <translation>Montrer le Debug dans le Casque RV</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="620"/>
        <source>Now</source>
        <translation>Maintenant</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="725"/>
        <location filename="../src/SettingsWidget.cpp" line="755"/>
        <source>Main</source>
        <translation>Principale</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="730"/>
        <location filename="../src/SettingsWidget.cpp" line="760"/>
        <location filename="../src/SettingsWidget.cpp" line="787"/>
        <source>Subwindow </source>
        <translation>Sous-fenêtre </translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="739"/>
        <source>Remove Window</source>
        <translation>Supprimer la Fenêtre</translation>
    </message>
    <message>
        <location filename="../src/SettingsWidget.cpp" line="740"/>
        <source>Are you sure about removing this window ?</source>
        <translation>Voulez-vous vraiment supprimer cette fenêtre ?</translation>
    </message>
    <message>
        <location filename="../include/SettingsWidget.hpp" line="104"/>
        <source>Windows</source>
        <translation>Fenêtres</translation>
    </message>
    <message>
        <location filename="../include/SettingsWidget.hpp" line="112"/>
        <source>Language (needs restart)</source>
        <translation>Langage (nécessite un redémarrage)</translation>
    </message>
</context>
<context>
    <name>ShaderEditor</name>
    <message>
        <location filename="../src/gui/ShaderEditor.cpp" line="27"/>
        <source>Shader Editor</source>
        <translation>Éditeur de Shader</translation>
    </message>
    <message>
        <location filename="../src/gui/ShaderEditor.cpp" line="77"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../src/gui/ShaderEditor.cpp" line="90"/>
        <source>Reload</source>
        <translation>Recharger</translation>
    </message>
</context>
<context>
    <name>ShaderSelector</name>
    <message>
        <location filename="../src/gui/ShaderSelector.cpp" line="27"/>
        <source>Shader Selector</source>
        <translation>Sélecteur de Shader</translation>
    </message>
    <message>
        <location filename="../src/gui/ShaderSelector.cpp" line="35"/>
        <source>Search :</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <location filename="../src/gui/ShaderSelector.cpp" line="44"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/gui/ShaderSelector.cpp" line="53"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
</context>
<context>
    <name>TextureSelector</name>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="35"/>
        <source>Texture Selector</source>
        <translation>Sélecteur de Texture</translation>
    </message>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="43"/>
        <source>Search :</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="52"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="61"/>
        <source>View</source>
        <translation>Voir</translation>
    </message>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="128"/>
        <source>Unsupported texture type</source>
        <translation>Type de texture non supporté</translation>
    </message>
    <message>
        <location filename="../src/gui/textures/TextureSelector.cpp" line="129"/>
        <source>This texture type doesn&apos;t have a viewer implemented yet.</source>
        <translation>Ce type de texture n&apos;a pas encore de visualiseur implémenté.</translation>
    </message>
</context>
<context>
    <name>WindowParametersSelector</name>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="45"/>
        <source>Width :</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="57"/>
        <source>Height :</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="67"/>
        <source>Fullscreen :</source>
        <translation>Plein écran :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="91"/>
        <source>Screen :</source>
        <translation>Écran :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="106"/>
        <source>Horizontal Shift Angle :</source>
        <translation>Angle de Décalage Horizontal :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="121"/>
        <source>Vertical Shift Angle :</source>
        <translation>Angle de Décalage Vertical :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="131"/>
        <source>Force left eye rendering only :</source>
        <translation>Forcer le rendu de l&apos;œil gauche seulement :</translation>
    </message>
    <message>
        <location filename="../src/gui/WindowParametersSelector.cpp" line="141"/>
        <source>Force right eye rendering only :</source>
        <translation>Forcer le rendu de l&apos;œil droit seulement :</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="84"/>
        <source>By-pass launcher and launch application.</source>
        <translation>Lance l&apos;application directement sans lanceur.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="89"/>
        <source>Read .ini config from &lt;file&gt;.</source>
        <translation>Lit la configuration .ini depuis &lt;file&gt;.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="94"/>
        <source>Display version information.</source>
        <translation>Affiche les informations de version.</translation>
    </message>
</context>
</TS>
