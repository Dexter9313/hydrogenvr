#!/bin/bash

# This script will be called from MSYS2 UCRT64 environment
# Check https://www.msys2.org/docs/environments/ and https://packages.msys2.org/queue
#
# The easiest way to install dependencies is to use
# pacman --no-confirm --needed -S package_name
# from MSYS2, by far.
#
# You can also build packages from source or download them manually then manualle set
# package_INCLUDE_DIRS, package_LIBRARIES or whatever this way:
#
# #Considering we are in $APPVEYOR_BUILD_FOLDER
# echo "export package_INCLUDE_DIRS=..." >> ./deps/DEPENDENCIES_ENV
# echo "export package_LIBRARIES=..." >> ./deps/DEPENDENCIES_ENV
