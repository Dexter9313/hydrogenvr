/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DOWNLOADMANAGER_HPP
#define DOWNLOADMANAGER_HPP

#include <QElapsedTimer>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class QFile;
class QProgressDialog;
class QUrl;
class QWidget;

class NetworkReply : public QObject
{
	Q_OBJECT
  public:
	explicit NetworkReply(QNetworkReply* qtNetworkReply = nullptr);
	NetworkReply(NetworkReply const&)            = delete;
	NetworkReply& operator=(NetworkReply const&) = delete;
	NetworkReply(NetworkReply&& other) noexcept;
	NetworkReply& operator=(NetworkReply&& other) noexcept;
	QNetworkReply* getQtNetworkReply() { return qtNetworkReply; };
	// updated every second at most
	double getCurrentBytesPerSecond(qint64 totalBytesReceived) const;
	void wait() const;
	~NetworkReply()
	{
		if(doClean)
		{
			qtNetworkReply->deleteLater();
		}
	};

  private:
	bool doClean = true;

	QNetworkReply* qtNetworkReply = nullptr;
	QElapsedTimer timer;
	mutable qint64 sizeBack           = 0;
	mutable qint64 prevMSecs          = 0;
	mutable double currentBytesPerSec = 0.0;
};

class DownloadManager
{
  public:
	struct DownloadOptions
	{
		qint64 byteStart  = 0;
		qint64 byteEnd    = -1;
		bool resume       = true;
		bool showProgress = false;
	};

	// building blocks
	static qint64 getFileSize(QUrl const& url);
	static NetworkReply download(QUrl const& url, qint64 byteStart = 0,
	                             qint64 byteEnd = -1);
	static NetworkReply postREST(QUrl const& url,
	                             QMap<QByteArray, QByteArray> const& rawHeader,
	                             QJsonDocument const& data);
	static QJsonDocument replyToRESTReply(NetworkReply& reply);

	// higher level
	static NetworkReply downloadFileAsync(QUrl const& url, QFile& destFile,
	                                      DownloadOptions const& options);

	static QNetworkReply::NetworkError
	    downloadFileSync(QUrl const& url, QFile& destFile,
	                     DownloadOptions const& options,
	                     QWidget* parent = nullptr);

	static QJsonDocument getRESTSync(QUrl const& url);

  private:
	static QNetworkAccessManager& manager();
};

#endif // DOWNLOADMANAGER_HPP
