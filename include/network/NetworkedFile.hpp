/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NETWORKEDFILE_HPP
#define NETWORKEDFILE_HPP

#include <QFile>
#include <QIODevice>
#include <QUrl>

class NetworkedFile : public QIODevice
{
  public:
	NetworkedFile(QUrl url, QObject* parent);

	bool open(OpenMode mode) override;
	void close() override;
	qint64 pos() const override;
	bool seek(qint64 pos) override;
	qint64 size() const override;
	qint64 readData(char* data, qint64 maxlen) override;
	qint64 writeData(const char* data, qint64 len) override;
	bool isSequential() const override;

  private:
	bool local;

	qint64 cursor = 0;

	QUrl url;
	QFile localFile;
};

#endif // NETWORKEDFILE_HPP
