#!/bin/bash

if [ -f ./project_directory.conf ]; then
    . ./project_directory.conf
else
    PROJECT_DIRECTORY="example"
    HVR_DIRECTORY="."
fi
. ./${PROJECT_DIRECTORY}/build.conf

mkdir build ; cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DWERROR=true -DUSE_QT6=ON
export VERSION=$(cat PROJECT_VERSION)
make -j $(nproc)
make package
./$PROJECT_NAME --version
./tests

