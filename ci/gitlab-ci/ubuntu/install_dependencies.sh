#!/bin/bash

ubuntu_version=$(grep "VERSION_ID" /etc/os-release | cut -d '=' -f 2 | tr -d '"')
echo "Ubuntu version: $ubuntu_version"

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -yq build-essential wget cmake git libassimp-dev libopenvr-dev glslang-tools python3-dev libzstd-dev libgit2-dev pkg-config qt6-base-dev qt6-base-private-dev
if [[ "$ubuntu_version" == "22.04" ]]
then
	echo "22.04 -=-=-=-=-=-"
	DEBIAN_FRONTEND=noninteractive apt-get install -yq libqt6core5compat6-dev libgl1-mesa-dev
else # 24.04 or above
	echo "else -=-=-=-=-=-="
	DEBIAN_FRONTEND=noninteractive apt-get install -yq libquazip1-qt6-dev libpython3-dev
fi

mkdir deps ; cd deps
# install leap motion sdk
#wget --no-verbose --content-disposition https://warehouse.leapmotion.com/apps/4185/download ;
#tar xzf Leap_Motion_SDK_Linux_*.tgz ;
#cp ./LeapDeveloperKit*/LeapSDK/include/Leap*.h /usr/include ;
#cp ./LeapDeveloperKit*/LeapSDK/lib/x64/libLeap.so /usr/lib ;
# install libktx
git clone --branch v4.3.2 https://github.com/KhronosGroup/KTX-Software.git
mkdir KTX-Software/build ; cd KTX-Software/build
cmake .. -DKTX_FEATURE_STATIC_LIBRARY=ON -DKTX_FEATURE_TESTS=OFF -DKTX_FEATURE_TOOLS=OFF
make install -j $(nproc)
cd ../..

# PythonQt
##### proper solution for now: ISSUE https://github.com/MeVisLab/pythonqt/pull/242 PR https://github.com/MeVisLab/pythonqt/pull/242 
git clone https://github.com/wangdaye078/pythonqt.git
cd pythonqt
	git checkout opengl_compatible
	git remote add mevislab https://github.com/MeVisLab/pythonqt.git
	git pull --rebase mevislab master
	# solve conflict...
	sed -i '/<<<<<<< HEAD/,/>>>>>>>/c\
  <enum-type name="QPageLayout::OutOfBoundsPolicy" since-version="6.8"/>\
  <enum-type name="QOpenGLFramebufferObject::Attachment" before-version="6" />\
  <enum-type name="QOpenGLShader::ShaderTypeBit" flags="QOpenGLShader::ShaderType" before-version="6" />' generator/typesystem_gui.xml
	# block actually useless, cleanup just in case
	git add generator/typesystem_gui.xml
	git config user.email "dont.care@dc.dc"
	git config user.name "Dont Care"
	GIT_EDITOR=true git rebase --continue
	# end of useless block
	cd generator
		qmake6
		make -j $(nproc)
		mkdir qt_fake_dir
		ln -s /usr/include/x86_64-linux-gnu/qt6/ qt_fake_dir/include
		QTDIR=$(pwd)/qt_fake_dir ./pythonqt_generator
	cd ..
	find . -type f -exec sed -i 's|\$\${INSTALL_PREFIX}|/usr/local|g' {} +
	sed -i 's/ examples//' PythonQt.pro
	sed -i 's/ tests//' PythonQt.pro
	sed -i 's/ generator//' PythonQt.pro
	version=$(python3 -c 'import sys; print(".".join(sys.version.split(".")[:2]))')
	qmake6 PYTHON_VERSION=$version PYTHON_PATH=/usr/include/python$version CONFIG+=release
	make install -j $(nproc)
cd ..

### QtGamepad
git clone https://github.com/qt/qtgamepad.git
cd qtgamepad
	git checkout 6.3.0
	QT_SELECT=qt6 qmake6
	make install -j $(nproc)
cd ..

if [[ "$ubuntu_version" == "22.04" ]]
then
	### Quazip Qt6
	git clone https://github.com/stachenov/quazip.git
	cd quazip
		git checkout v1.4
		mkdir build
		cd build
			cmake ..
			make install -j $(nproc)
		cd ..
	cd ..
fi

cd ..
# install project additional deps
/project_install_dependencies.sh

rm -rf /deps
