#!/bin/bash

apt-get update ; apt-get install -y clang-format-19 clang-tidy-19
mkdir -p build ; cd build
cmake .. -DUSE_QT6=ON
make clang-format
make clang-tidy
