#!/bin/bash

# copies all dependency dlls for a file recursively
resolve_dependencies() {
	local file=$1
	ldd "$file" | grep -v "=> /c/" | awk '{print $3}' | sort | uniq | while read -r dep; do
		# check if the dependency is a file and not already copied
		if [ -f "$dep" ] && [ ! -f "$release_dir/$(basename "$dep")" ]; then
			echo "Copying $dep to $release_dir"
			cp "$dep" "$release_dir/"
			resolve_dependencies "$dep"
		fi
	done
}

main_dir=$(pwd)

build_dir=$(pwd)/build
version=$(cat $build_dir/PROJECT_VERSION)
release_dir_name=$PROJECT_NAME-$version-windows-64bit
release_dir=$(pwd)/$release_dir_name

mkdir -p $release_dir
cd $release_dir

# copy exe and Qt dlls+plugins
cp $build_dir/$PROJECT_NAME.exe .

mkdir $release_dir/platforms
mkdir $release_dir/imageformats
mkdir $release_dir/audio
mkdir $release_dir/mediaservice
mkdir $release_dir/gamepads
cp /ucrt64/share/qt5/plugins/platforms/qwindows.dll $release_dir/platforms
cp /ucrt64/share/qt5/plugins/imageformats/*.dll $release_dir/imageformats
cp /ucrt64/share/qt5/plugins/audio/*.dll $release_dir/audio
cp /ucrt64/share/qt5/plugins/mediaservice/*.dll $release_dir/mediaservice
cp /ucrt64/share/qt5/plugins/gamepads/*.dll $release_dir/gamepads

# resolve dependencies for Qt plugins
for d in */; do
	if [ -d "$d" ]; then
		for dll in "$d"/*.dll; do
			if [ -f "$dll" ]; then
				resolve_dependencies "$dll"
			fi
		done
	fi
done

# resolve dependencies for the main .exe and copy data/ dir
resolve_dependencies $PROJECT_NAME.exe
cp -r $build_dir/data .

cd $build_dir

# python
cp $PYTHON_PATH/python310.dll $release_dir
curl -L "$PYTHON_URL" -o python.zip
unzip -q python.zip -d python
rm -f python.zip
unzip -q python/python310.zip -d "$release_dir/python"
mv python/*.pyd "$release_dir/python/"

cd $main_dir
zip -r $release_dir_name.zip $release_dir_name
