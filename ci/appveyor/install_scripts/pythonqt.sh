#!/bin/bash

cd deps

if [[ ! -d pythonqt ]]
then
	git clone --branch v3.5.6 https://github.com/MeVisLab/pythonqt.git
	cd pythonqt

	# install libs in the right path
	PRO_FILE="src/src.pro"
	sed -i '/unix: target.path/d' "$PRO_FILE"
	sed -i '/win32: target.path/d' "$PRO_FILE"
	echo 'target.path = $${INSTALL_PREFIX}/lib' >> "$PRO_FILE"
	PRO_FILE="extensions/PythonQt_QtAll/PythonQt_QtAll.pro"
	sed -i '/unix: target.path/d' "$PRO_FILE"
	sed -i '/win32: target.path/d' "$PRO_FILE"
	echo 'target.path = $${INSTALL_PREFIX}/lib' >> "$PRO_FILE"

	qmake "INSTALL_PREFIX=/ucrt64"
	make -j $(nproc)
	cd ..
fi

cd pythonqt
make install
mv /ucrt64/lib/*.dll /ucrt64/bin
echo "export PYTHONQT_INCLUDE_DIRS=/ucrt64/include" >> ../DEPENDENCIES_ENV
echo "export PYTHONQT_LIBRARIES=PythonQt-Qt5-Python3.10" >> ../DEPENDENCIES_ENV
echo "export PYTHONQT_QTALL_INCLUDE_DIRS=/ucrt64/include" >> ../DEPENDENCIES_ENV
echo "export PYTHONQT_QTALL_LIBRARIES=PythonQt_QtAll-Qt5-Python3.10" >> ../DEPENDENCIES_ENV
cd ..
