#!/bin/bash

cd deps

if [[ ! -d KTX-Software ]]
then
	git clone --branch v4.3.2 https://github.com/KhronosGroup/KTX-Software.git
	mkdir KTX-Software/build ; cd KTX-Software/build
	cmake .. -G "Unix Makefiles" -DKTX_FEATURE_TESTS=OFF -DKTX_FEATURE_TOOLS=OFF -DCMAKE_INSTALL_PREFIX=/ucrt64
	make -j $(nproc)
	cd ../..
fi

cd KTX-Software/build
make install
echo "export LIBKTX_INCLUDE_DIRS=/ucrt64/include" >> ../../DEPENDENCIES_ENV
echo "export LIBKTX_LIBRARIES=ktx" >> ../../DEPENDENCIES_ENV
cd ..
