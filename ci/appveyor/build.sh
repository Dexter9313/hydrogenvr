#/bin/bash

. deps/DEPENDENCIES_ENV

mkdir build
cd build
cmake ..
ninja
