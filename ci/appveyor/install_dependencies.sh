#!/bin/bash

pacman -S --noconfirm --needed pkgconf git zip make mingw-w64-ucrt-x86_64-gcc mingw-w64-ucrt-x86_64-dlfcn mingw-w64-ucrt-x86_64-cmake mingw-w64-ucrt-x86_64-assimp mingw-w64-ucrt-x86_64-openvr mingw-w64-ucrt-x86_64-libgit2 mingw-w64-ucrt-x86_64-qt5-base mingw-w64-ucrt-x86_64-qt5-gamepad mingw-w64-ucrt-x86_64-quazip-qt5 mingw-w64-ucrt-x86_64-qt5-imageformats mingw-w64-ucrt-x86_64-qt5-tools

mkdir -p deps
./$HVR_DIRECTORY/ci/appveyor/install_scripts/libktx.sh
./$HVR_DIRECTORY/ci/appveyor/install_scripts/pythonqt.sh

# project dependencies
./$PROJECT_DIRECTORY/ci/appveyor/install_dependencies.sh
