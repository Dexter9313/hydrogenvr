/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "network/DownloadManager.hpp"

#include <QCoreApplication>
#include <QElapsedTimer>
#include <QFile>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QProgressDialog>
#include <QThread>
#include <cmath>

QNetworkAccessManager& DownloadManager::manager()
{
	static QNetworkAccessManager manager;
	return manager;
}

NetworkReply::NetworkReply(QNetworkReply* qtNetworkReply)
    : qtNetworkReply(qtNetworkReply)
{
	timer.start();
}

NetworkReply::NetworkReply(NetworkReply&& other) noexcept
    : qtNetworkReply(other.qtNetworkReply)
    , timer(other.timer)
    , sizeBack(other.sizeBack)
    , prevMSecs(other.prevMSecs)
    , currentBytesPerSec(other.currentBytesPerSec)
{
	other.doClean = false;
}

NetworkReply& NetworkReply::operator=(NetworkReply&& other) noexcept
{
	if(this == &other)
	{
		return *this;
	}

	qtNetworkReply     = other.qtNetworkReply;
	timer              = other.timer;
	sizeBack           = other.sizeBack;
	prevMSecs          = other.prevMSecs;
	currentBytesPerSec = other.currentBytesPerSec;
	other.doClean      = false;
	return *this;
}

double NetworkReply::getCurrentBytesPerSecond(qint64 totalBytesReceived) const
{
	auto msecs = timer.elapsed();
	auto dt    = (msecs - prevMSecs) / 1000.0;
	if(dt < 1.0)
	{
		return currentBytesPerSec;
	}
	prevMSecs = msecs;

	currentBytesPerSec = (totalBytesReceived - sizeBack) / dt;
	sizeBack           = totalBytesReceived;
	return currentBytesPerSec;
}

void NetworkReply::wait() const
{
	while(!qtNetworkReply->isFinished())
	{
		QCoreApplication::processEvents();
		QThread::msleep(10);
	}
}

qint64 DownloadManager::getFileSize(QUrl const& url)
{
	const QNetworkRequest request(url);
	QNetworkReply* reply = manager().head(request);

	while(!reply->isFinished())
	{
		QCoreApplication::processEvents();
	}

	if(reply->error() != QNetworkReply::NoError)
	{
		return -1;
	}

	const qint64 size
	    = reply->header(QNetworkRequest::ContentLengthHeader).toLongLong();
	reply->deleteLater();
	return size;
}

NetworkReply DownloadManager::download(QUrl const& url, qint64 byteStart,
                                       qint64 byteEnd)
{
	QNetworkRequest request(url);

	if(byteStart > 0 || byteEnd >= 0)
	{
		request.setRawHeader(
		    "Range", QByteArray("bytes=")
		                 + (byteStart > 0 ? QByteArray::number(byteStart)
		                                  : QByteArray{""})
		                 + "-"
		                 + (byteEnd >= 0 ? QByteArray::number(byteEnd)
		                                 : QByteArray{""}));
	}

	return NetworkReply{manager().get(request)};
}

NetworkReply
    DownloadManager::postREST(QUrl const& url,
                              QMap<QByteArray, QByteArray> const& rawHeader,
                              QJsonDocument const& data)
{
	QNetworkRequest request(url);
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
	for(auto const& key : rawHeader.keys())
	{
		request.setRawHeader(key, rawHeader[key]);
	}
	return NetworkReply{manager().post(request, data.toJson())};
}

QJsonDocument DownloadManager::replyToRESTReply(NetworkReply& reply)
{
	if(!reply.getQtNetworkReply()->isFinished())
	{
		return {};
	}
	return QJsonDocument::fromJson(reply.getQtNetworkReply()->readAll());
}

NetworkReply DownloadManager::downloadFileAsync(QUrl const& url,
                                                QFile& destFile,
                                                DownloadOptions const& options)
{
	qint64 byteStart = options.byteStart;

	auto remoteSize(getFileSize(url));
	if(options.resume && destFile.exists())
	{
		auto localSize(destFile.size());
		if(localSize == remoteSize)
		{
			qDebug() << "Already downloaded";
			return NetworkReply{};
		}
		byteStart += destFile.size();
	}
	else
	{
		destFile.close(); // Will open in append mode later
	}

	NetworkReply reply
	    = DownloadManager::download(url, byteStart, options.byteEnd);
	auto* qtReply = reply.getQtNetworkReply();
	QObject::connect(qtReply, &QNetworkReply::readyRead,
	                 [&destFile, qtReply]()
	                 {
		                 if(!destFile.isOpen())
		                 {
			                 destFile.open(QIODevice::Append);
		                 }
		                 destFile.write(qtReply->readAll());
	                 });

	return reply;
}

QNetworkReply::NetworkError
    DownloadManager::downloadFileSync(QUrl const& url, QFile& destFile,
                                      DownloadOptions const& options,
                                      QWidget* parent)
{
	std::unique_ptr<QProgressDialog> progressDialog;
	bool cancel = false;
	auto remoteSize(getFileSize(url));
	if(options.showProgress)
	{
		progressDialog = std::make_unique<QProgressDialog>(parent);
		progressDialog->setMaximum(remoteSize / 1024 / 1024);
		progressDialog->setWindowTitle(QObject::tr("Downloading..."));
		progressDialog->show();

		QObject::connect(progressDialog.get(), &QProgressDialog::canceled,
		                 [&cancel]() { cancel = true; });
	}
	NetworkReply reply = downloadFileAsync(url, destFile, options);
	if(reply.getQtNetworkReply() == nullptr)
	{
		return QNetworkReply::NoError;
	}

	while(!reply.getQtNetworkReply()->isFinished() && !cancel)
	{
		QCoreApplication::processEvents();
		QThread::msleep(10);
		if(options.showProgress)
		{
			const auto fileSize(destFile.size());
			auto currentBytesPerSec = reply.getCurrentBytesPerSecond(fileSize);

			progressDialog->setValue(fileSize / 1024 / 1024);
			const int remaining = static_cast<int>(
			    round((remoteSize - fileSize) / currentBytesPerSec));

			const QString dialogText
			    = QString::number(fileSize / 1024.0 / 1024 / 1024, 'g', 2)
			      + "GiB/"
			      + QString::number(remoteSize / 1024.0 / 1024 / 1024, 'g', 2)
			      + "GiB "
			      + QString::number(currentBytesPerSec / 1024 / 1024, 'g', 3)
			      + " MiB/s ETA: "
			      + QTime(0, 0).addSecs(remaining).toString("hh:mm:ss")
			      + " secs";
			progressDialog->setLabelText(dialogText);
		}
	}

	if(cancel)
	{
		reply.getQtNetworkReply()->abort();
		return QNetworkReply::NoError;
	}

	return reply.getQtNetworkReply()->error();
}

QJsonDocument DownloadManager::getRESTSync(QUrl const& url)
{
	auto reply = download(url);
	reply.wait();
	return replyToRESTReply(reply);
}
