/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "network/NetworkedFile.hpp"

#include <QCoreApplication>
#include <QThread>

#include "network/DownloadManager.hpp"

NetworkedFile::NetworkedFile(QUrl url, QObject* parent)
    : QIODevice(parent)
    , local(url.isValid() && url.isLocalFile())
    , url(std::move(url))
{
	if(local)
	{
		localFile.setFileName(this->url.toLocalFile());
	}
}

bool NetworkedFile::open(OpenMode mode)
{
	if(local && !localFile.open(mode))
	{
		return false;
	}

	if(!url.isValid())
	{
		return false;
	}
	return QIODevice::open(mode);
}

void NetworkedFile::close()
{
	if(local)
	{
		localFile.close();
	}
	QIODevice::close();
}

qint64 NetworkedFile::pos() const
{
	if(local)
	{
		return localFile.pos();
	}
	return cursor;
}

bool NetworkedFile::seek(qint64 pos)
{
	if(local)
	{
		return localFile.seek(pos);
	}
	cursor = pos;
	return true;
}

qint64 NetworkedFile::size() const
{
	if(local)
	{
		return localFile.size();
	}

	if(url.isValid())
	{
		return DownloadManager::getFileSize(url);
	}

	return -1;
}

qint64 NetworkedFile::readData(char* data, qint64 maxlen)
{
	if(local)
	{
		return localFile.read(data, maxlen);
	}
	auto reply    = DownloadManager::download(url, cursor, cursor + maxlen);
	auto* qtReply = reply.getQtNetworkReply();
	while(!qtReply->isFinished())
	{
		QCoreApplication::processEvents();
		QThread::msleep(10);
	}

	auto result = qtReply->readAll();
	memcpy(data, result.data(),
	       result.size() > maxlen ? maxlen : result.size());
	return result.size();
}

qint64 NetworkedFile::writeData(char const* /*data*/, qint64 /*len*/)
{
	return -1;
}

bool NetworkedFile::isSequential() const
{
	return false;
}
