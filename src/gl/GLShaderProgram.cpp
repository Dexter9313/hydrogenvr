/*
    Copyright (C) 2020 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gl/GLHandler.hpp"

#include "gl/GLShaderProgram.hpp"

#include <QOpenGLFunctions>

unsigned int& GLShaderProgram::instancesCount()
{
	static unsigned int instancesCount = 0;
	return instancesCount;
}

GLShaderProgram::GLShaderProgram(GLShaderProgram&& other) noexcept
    : glShaderProgram(other.glShaderProgram)
    , uniformLocations(std::move(other.uniformLocations))
    , doClean(other.doClean)
{
	// prevent other from cleaning shader if it destroys itself
	other.doClean = false;
}

GLShaderProgram& GLShaderProgram::operator=(GLShaderProgram&& other) noexcept
{
	if(this == &other)
	{
		return *this;
	}
	cleanUp();

	glShaderProgram  = other.glShaderProgram;
	uniformLocations = other.uniformLocations;
	doClean          = other.doClean;

	other.doClean = false;
	return *this;
}

GLShaderProgram::GLShaderProgram(QString const& shadersCommonName,
                                 QMap<QString, QString> const& defines)
    : GLShaderProgram(shadersCommonName, shadersCommonName, defines)
{
}

GLShaderProgram::GLShaderProgram(QString const& vertexName,
                                 QString const& fragmentName,
                                 QMap<QString, QString> const& defines)
    : GLShaderProgram(
          {{vertexName, Stage::VERTEX}, {fragmentName, Stage::FRAGMENT}},
          defines)
{
}

GLShaderProgram::GLShaderProgram(
    std::vector<std::pair<QString, Stage>> const& pipeline,
    QMap<QString, QString> const& defines)
    : glShaderProgram(GLHandler::glf().glCreateProgram())
{
	++instancesCount();

	auto fullName = "Program " + QString::number(glShaderProgram) + " - ";
	for(auto const& stage : pipeline)
	{
		QString name(stage.first);
		fullName += name + ' ';
		// (extension, GLenum stage)
		auto pair(decodeStage(stage.second));
		if(!name.contains('.'))
		{
			name = "shaders/" + name + pair.first;
		}
		// vertex shader
		const GLuint shader(loadShader(name, pair.second, defines));
		GLHandler::glf().glAttachShader(glShaderProgram, shader);
		GLHandler::glf().glDeleteShader(shader);
	}
	GLHandler::glf().glObjectLabel(GL_PROGRAM, glShaderProgram, fullName.size(),
	                               fullName.toLatin1().data());

	GLHandler::glf().glBindFragDataLocation(
	    glShaderProgram, 0,
	    "outColor"); // optional for one buffer
	GLHandler::glf().glLinkProgram(glShaderProgram);
	GLHandler::glf().glValidateProgram(glShaderProgram);

	// populate uniformLocations
	GLint numUniforms = 0;
	GLHandler::glf().glGetProgramiv(glShaderProgram, GL_ACTIVE_UNIFORMS,
	                                &numUniforms);
	for(GLint i = 0; i < numUniforms; ++i)
	{
		std::array<char, 256> name{};
		GLsizei length = 0;
		GLint size     = 0;
		GLenum type    = 0;

		GLHandler::glf().glGetActiveUniform(glShaderProgram, i, sizeof(name),
		                                    &length, &size, &type, name.data());
		QString nameStr = name.data();
		if(nameStr.endsWith("[0]"))
		{
			nameStr.chop(3);
		}
		uniformLocations.insert(nameStr, GLHandler::glf().glGetUniformLocation(
		                                     glShaderProgram, name.data()));
	}
}

void GLShaderProgram::cleanUp()
{
	if(!doClean)
	{
		return;
	}
	--instancesCount();
	GLHandler::glf().glUseProgram(0);
	GLHandler::glf().glDeleteProgram(glShaderProgram);
	doClean = false;
}

int GLShaderProgram::getAttribLocationFromName(const char* attributeName) const
{
	return GLHandler::glf().glGetAttribLocation(glShaderProgram, attributeName);
}

int GLShaderProgram::getUniformLocation(const char* uniformName) const
{
	if(uniformLocations.contains(uniformName))
	{
		return uniformLocations[uniformName];
	}
	return -1;
}

void GLShaderProgram::setUnusedAttributesValues(
    std::vector<QPair<QString, std::vector<float>>> const& defaultValues) const
{
	for(auto attribute : defaultValues)
	{
		const GLint posAttrib = GLHandler::glf().glGetAttribLocation(
		    glShaderProgram, attribute.first.toLatin1().data());
		if(posAttrib != -1)
		{
			GLHandler::glf().glDisableVertexAttribArray(posAttrib);
			// special case where we have to do it, see :
			// https://bugreports.qt.io/browse/QTBUG-40090?jql=text%20~%20%22glvertexattrib%22
			QOpenGLFunctions glf_base;
			glf_base.initializeOpenGLFunctions();
			switch(attribute.second.size())
			{
				case 1:
					glf_base.glVertexAttrib1fv(posAttrib,
					                           attribute.second.data());
					break;
				case 2:
					glf_base.glVertexAttrib2fv(posAttrib,
					                           attribute.second.data());
					break;
				case 3:
					glf_base.glVertexAttrib3fv(posAttrib,
					                           attribute.second.data());
					break;
				case 4:
					glf_base.glVertexAttrib4fv(posAttrib,
					                           attribute.second.data());
					break;
				default:
					break;
			}
		}
	}
}

void GLShaderProgram::setUnusedAttributesValues(
    QStringList const& names,
    std::vector<std::vector<float>> const& values) const
{
	std::vector<QPair<QString, std::vector<float>>> defaultValues;
	defaultValues.reserve(values.size());
	for(unsigned int i(0); i < values.size(); ++i)
	{
		defaultValues.emplace_back(names[i].toLatin1().constData(), values[i]);
	}
	setUnusedAttributesValues(defaultValues);
}

void GLShaderProgram::setUniform(const char* uniformName,
                                 unsigned int value) const
{
	GLHandler::glf().glProgramUniform1ui(
	    glShaderProgram, getUniformLocation(uniformName), value);
}

void GLShaderProgram::setUniform(const char* uniformName, int value) const
{
	GLHandler::glf().glProgramUniform1i(glShaderProgram,
	                                    getUniformLocation(uniformName), value);
}

void GLShaderProgram::setUniform(const char* uniformName, unsigned int size,
                                 int const* values) const
{
	GLHandler::glf().glProgramUniform1iv(
	    glShaderProgram, getUniformLocation(uniformName), size, values);
}

void GLShaderProgram::setUniform(const char* uniformName, float value) const
{
	GLHandler::glf().glProgramUniform1f(glShaderProgram,
	                                    getUniformLocation(uniformName), value);
}

void GLShaderProgram::setUniform(const char* uniformName, unsigned int size,
                                 float const* values) const
{
	GLHandler::glf().glProgramUniform1fv(
	    glShaderProgram, getUniformLocation(uniformName), size, values);
}

void GLShaderProgram::setUniform(const char* uniformName,
                                 QVector2D const& value) const
{
	GLHandler::glf().glProgramUniform2f(
	    glShaderProgram, getUniformLocation(uniformName), value.x(), value.y());
}

void GLShaderProgram::setUniform(const char* uniformName,
                                 QVector3D const& value) const
{
	GLHandler::glf().glProgramUniform3f(glShaderProgram,
	                                    getUniformLocation(uniformName),
	                                    value.x(), value.y(), value.z());
}

void GLShaderProgram::setUniform(const char* uniformName, unsigned int size,
                                 QVector3D const* values) const
{
	std::vector<GLfloat> data(3 * size);
	for(unsigned int i(0); i < size; ++i)
	{
		for(unsigned int j(0); j < 3; ++j)
		{
			data[i * 3 + j] = values[i][j];
		}
	}
	GLHandler::glf().glProgramUniform3fv(
	    glShaderProgram, getUniformLocation(uniformName), size, data.data());
}

void GLShaderProgram::setUniform(const char* uniformName,
                                 QVector4D const& value) const
{
	GLHandler::glf().glProgramUniform4f(
	    glShaderProgram, getUniformLocation(uniformName), value.x(), value.y(),
	    value.z(), value.w());
}

void GLShaderProgram::setUniform(const char* uniformName, unsigned int size,
                                 QVector4D const* values) const
{
	std::vector<GLfloat> data(4 * size);
	for(unsigned int i(0); i < size; ++i)
	{
		for(unsigned int j(0); j < 4; ++j)
		{
			data[i * 4 + j] = values[i][j];
		}
	}
	GLHandler::glf().glProgramUniform4fv(
	    glShaderProgram, getUniformLocation(uniformName), size, data.data());
}

void GLShaderProgram::setUniform(const char* uniformName,
                                 QMatrix4x4 const& value) const
{
	GLHandler::glf().glProgramUniformMatrix4fv(glShaderProgram,
	                                           getUniformLocation(uniformName),
	                                           1, GL_FALSE, value.data());
}

void GLShaderProgram::setUniform(const char* uniformName, unsigned int size,
                                 QMatrix4x4 const* values) const
{
	std::vector<GLfloat> data(16 * size);
	for(unsigned int i(0); i < size; ++i)
	{
		for(unsigned int j(0); j < 16; ++j)
		{
			data[i * 16 + j] = values[i].data()[j];
		}
	}
	GLHandler::glf().glProgramUniformMatrix4fv(glShaderProgram,
	                                           getUniformLocation(uniformName),
	                                           size, GL_FALSE, data.data());
}

void GLShaderProgram::setUniform(const char* uniformName, QColor const& value,
                                 bool sRGB) const
{
	const QColor linVal(sRGB ? GLHandler::sRGBToLinear(value) : value);
	setUniform(uniformName,
	           QVector3D(linVal.redF(), linVal.greenF(), linVal.blueF()));
}

void GLShaderProgram::use() const
{
	GLHandler::glf().glUseProgram(glShaderProgram);
}

void GLShaderProgram::get(GLenum pname, GLint* params) const
{
	GLHandler::glf().glGetProgramiv(glShaderProgram, pname, params);
}

std::pair<QString, GLenum> GLShaderProgram::decodeStage(Stage s)
{
	switch(s)
	{
		case Stage::VERTEX:
			return {".vert", GL_VERTEX_SHADER};
		case Stage::TESS_CONTROL:
			return {".tesc", GL_TESS_CONTROL_SHADER};
		case Stage::TESS_EVALUATION:
			return {".tese", GL_TESS_EVALUATION_SHADER};
		case Stage::GEOMETRY:
			return {".geom", GL_GEOMETRY_SHADER};
		case Stage::FRAGMENT:
			return {".frag", GL_FRAGMENT_SHADER};
		case Stage::COMPUTE:
			return {".comp", GL_COMPUTE_SHADER};
		default:
			return {};
	}
}

QString GLShaderProgram::getFullPreprocessedSource(
    QString const& path, QMap<QString, QString> const& defines,
    std::vector<QString>& debugFiles)
{
	const size_t id(debugFiles.size());
	debugFiles.push_back(path);
	// Read source
	QFile f(utils::getAbsoluteDataPath(path));
	if(!f.exists())
	{
		f.setFileName(utils::getAbsoluteDataPath("shaders/" + path));
		if(!f.exists())
		{
			qWarning() << "Shader not loaded :\"" << path << "\" not found";
			return "///!ERROR";
		}
	}
	f.open(QFile::ReadOnly | QFile::Text);
	QTextStream in(&f);
	QString source(in.readAll().toLocal8Bit());

	// add source name at the beginning and end of the file
	source.insert(source.indexOf('\n'), " ///!BEGSRC " + path);
	source.insert(source.lastIndexOf('\n'), QString(" ///!ENDSRC"));

	// include other preprocessed sources within source
	int includePos(source.lastIndexOf("#include"));
	while(includePos != -1)
	{
		const int beginPath(source.indexOf('<', includePos));
		const int endPath(source.indexOf('>', includePos));
		const int endOfLine(source.indexOf('\n', includePos) + 1);

		const QString includePath(
		    source.mid(beginPath + 1, endPath - beginPath - 1));

		QString includedSrc(
		    getFullPreprocessedSource(includePath, {}, debugFiles));

		const unsigned int line(source.left(includePos).count('\n'));

		if(includedSrc == "///!ERROR")
		{
			QString warning("SHADER ERROR (");
			warning += path + ":" + QString::number(line + 1) + ") : ";
			warning += "include error : '" + includePath + "' does not exist";
			qWarning() << warning;
		}

		includedSrc += "#line " + QString::number(line) + ' '
		               + QString::number(id) + '\n';

		source.replace(includePos, endOfLine - includePos, includedSrc);

		includePos = source.lastIndexOf("#include", includePos);
	}

	// add defines after #version
	int definesInsertPoint(source.indexOf("#version"));
	if(definesInsertPoint == -1)
	{
		definesInsertPoint = 0;
	}
	else
	{
		definesInsertPoint = source.indexOf('\n', definesInsertPoint) + 1;
	}

	const unsigned int line(source.left(definesInsertPoint).count('\n') - 1);
	source.insert(definesInsertPoint, "#line " + QString::number(line) + ' '
	                                      + QString::number(id) + '\n');
	for(auto const& key : defines.keys())
	{
		source.insert(definesInsertPoint, QString("#define ") + key + " "
		                                      + defines.value(key) + "\n");
	}

	return source;
}

GLuint GLShaderProgram::loadShader(QString const& path, GLenum shaderType,
                                   QMap<QString, QString> const& defines)
{
	std::vector<QString> debugFiles;
	const QString source(getFullPreprocessedSource(path, defines, debugFiles));

	QByteArray ba     = source.toLatin1();
	const char* bytes = ba.data();

	const GLuint shader = GLHandler::glf().glCreateShader(shaderType);
	auto fullName       = "Shader " + QString::number(shader) + " - " + path;
	GLHandler::glf().glObjectLabel(GL_SHADER, shader, fullName.size(),
	                               fullName.toLatin1().data());
	GLHandler::glf().glShaderSource(shader, 1, &bytes, nullptr);
	GLHandler::glf().glCompileShader(shader);

	// checks
	GLint status = 0;
	GLHandler::glf().glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	std::array<char, 512> buffer = {};
	GLHandler::glf().glGetShaderInfoLog(shader, 512, nullptr, buffer.data());
	if(status != GL_TRUE)
	{
		const QString bufStrs(buffer.data());
		for(auto const& bufStr : bufStrs.split('\n'))
		{
			if(bufStr.isEmpty())
			{
				continue;
			}
			const int lineBeg       = bufStr.indexOf('(') + 1;
			const int lineSize      = bufStr.indexOf(')') - lineBeg;
			const int msgBeg        = bufStr.indexOf(':') + 2;
			const unsigned int file = bufStr.left(lineBeg - 1).toInt();
			const unsigned int line = bufStr.mid(lineBeg, lineSize).toInt();
			QString warning("SHADER ERROR (");
			warning += QString::number(shader) + " - ";
			warning += debugFiles[file] + ":" + QString::number(line) + ") : ";
			warning += bufStr.mid(msgBeg).replace('"', '\'');
			qWarning() << warning;
		}
	}

	return shader;
}
