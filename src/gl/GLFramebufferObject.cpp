/*
    Copyright (C) 2020 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include "gl/GLHandler.hpp"

#include "gl/GLFramebufferObject.hpp"

#include <QScreen>
#include <QWindow>

unsigned int& GLFramebufferObject::instancesCount()
{
	static unsigned int instancesCount = 0;
	return instancesCount;
}

QList<QPair<GLint, GLint>>& GLFramebufferObject::bindStack()
{
	static QList<QPair<GLint, GLint>> bindStack;
	return bindStack;
}

GLFramebufferObject::GLFramebufferObject(GLTexture&& colorAttachment, int level)
    : width(colorAttachment.getSize()[0])
    , height(colorAttachment.getSize()[1])
    , depth(colorAttachment.getSize()[2])
{
	++instancesCount();

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// bind texture to framebuffer
	texColorBuffer = std::make_unique<GLTexture>(std::move(colorAttachment));
	GLHandler::glf().glFramebufferTexture2D(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texColorBuffer->getGLTarget(),
	    texColorBuffer->getGLTexture(), level);
}

GLFramebufferObject::GLFramebufferObject(GLFramebufferObject&& other) noexcept
    : fbo(other.fbo)
    , texColorBuffer(std::move(other.texColorBuffer))
    , renderBuffer(other.renderBuffer)
    , width(other.width)
    , height(other.height)
    , depth(other.depth)
    , isDepthMap(other.isDepthMap)
    , doClean(other.doClean)
{
	// prevent other from cleaning fbo if it destroys itself
	other.doClean = false;
}

GLFramebufferObject&
    GLFramebufferObject::operator=(GLFramebufferObject&& other) noexcept
{
	if(this == &other)
	{
		return *this;
	}
	cleanUp();

	fbo            = other.fbo;
	texColorBuffer = std::move(other.texColorBuffer);
	renderBuffer   = other.renderBuffer;
	width          = other.width;
	height         = other.height;
	depth          = other.depth;
	isDepthMap     = other.isDepthMap;
	doClean        = other.doClean;

	other.doClean = false;
	return *this;
}

GLFramebufferObject::GLFramebufferObject(
    GLTexture::Tex1DProperties const& properties,
    GLTexture::Sampler const& sampler)
    : width(properties.width)
{
	++instancesCount();

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// generate texture
	texColorBuffer = std::make_unique<GLTexture>(properties, sampler);
	GLHandler::glf().glFramebufferTexture1D(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texColorBuffer->getGLTarget(),
	    texColorBuffer->getGLTexture(), 0);
}

GLFramebufferObject::GLFramebufferObject(
    GLTexture::Tex2DProperties const& properties,
    GLTexture::Sampler const& sampler)
    : width(properties.width)
    , height(properties.height)
{
	++instancesCount();
	if(properties.internalFormat == GL_DEPTH_COMPONENT
	   || properties.internalFormat == GL_DEPTH_COMPONENT16
	   || properties.internalFormat == GL_DEPTH_COMPONENT24
	   || properties.internalFormat == GL_DEPTH_COMPONENT32)
	{
		isDepthMap = true;
	}

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	if(isDepthMap)
	{
		// generate texture
		texColorBuffer = std::make_unique<GLTexture>(
		    properties, sampler,
		    GLTexture::Data{nullptr, GL_FLOAT, GL_DEPTH_COMPONENT});

		// add depth specific texture parameters for sampler2DShadow
		GLHandler::glf().glBindTexture(GL_TEXTURE_2D,
		                               texColorBuffer->getGLTexture());
		GLHandler::glf().glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
		                                 GL_COMPARE_REF_TO_TEXTURE);
		GLHandler::glf().glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC,
		                                 GL_LEQUAL);

		GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		GLHandler::glf().glFramebufferTexture2D(
		    GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		    texColorBuffer->getGLTexture(), 0);
		GLHandler::glf().glDrawBuffer(GL_NONE);
		GLHandler::glf().glReadBuffer(GL_NONE);
		GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		// generate texture
		texColorBuffer = std::make_unique<GLTexture>(properties, sampler);
		GLHandler::glf().glFramebufferTexture2D(
		    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texColorBuffer->getGLTarget(),
		    texColorBuffer->getGLTexture(), 0);

		// render buffer for depth and stencil
		GLHandler::glf().glGenRenderbuffers(1, &renderBuffer);
		GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
		GLHandler::glf().glRenderbufferStorage(
		    GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
		GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, 0);

		// attach it to currently bound framebuffer object
		GLHandler::glf().glFramebufferRenderbuffer(
		    GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER,
		    renderBuffer);
	}
}

GLFramebufferObject::GLFramebufferObject(
    GLTexture::TexMultisampleProperties const& properties,
    GLTexture::Sampler const& sampler)
    : width(properties.width)
    , height(properties.height)
{
	++instancesCount();

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// generate texture
	texColorBuffer = std::make_unique<GLTexture>(properties, sampler);
	GLHandler::glf().glFramebufferTexture2D(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texColorBuffer->getGLTarget(),
	    texColorBuffer->getGLTexture(), 0);

	// render buffer for depth and stencil
	GLHandler::glf().glGenRenderbuffers(1, &renderBuffer);
	GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
	GLHandler::glf().glRenderbufferStorageMultisample(
	    GL_RENDERBUFFER, properties.samples, GL_DEPTH24_STENCIL8, width,
	    height);
	GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// attach it to currently bound framebuffer object
	GLHandler::glf().glFramebufferRenderbuffer(GL_FRAMEBUFFER,
	                                           GL_DEPTH_STENCIL_ATTACHMENT,
	                                           GL_RENDERBUFFER, renderBuffer);
}

GLFramebufferObject::GLFramebufferObject(
    GLTexture::Tex3DProperties const& properties,
    GLTexture::Sampler const& sampler)
    : width(properties.width)
    , height(properties.height)
    , depth(properties.depth)
{
	++instancesCount();

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// generate texture
	texColorBuffer = std::make_unique<GLTexture>(properties, sampler);
	GLHandler::glf().glFramebufferTexture3D(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texColorBuffer->getGLTarget(),
	    texColorBuffer->getGLTexture(), 0, 0);
}

GLFramebufferObject::GLFramebufferObject(
    GLTexture::TexCubemapProperties const& properties,
    GLTexture::Sampler const& sampler)
    : width(properties.side)
    , height(properties.side)
{
	++instancesCount();

	GLHandler::glf().glGenFramebuffers(1, &fbo);
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// generate texture
	texColorBuffer = std::make_unique<GLTexture>(properties, sampler);
	GLHandler::glf().glFramebufferTexture2D(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	    texColorBuffer->getGLTexture(), 0);

	// render buffer for depth and stencil
	GLHandler::glf().glGenRenderbuffers(1, &renderBuffer);
	GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
	GLHandler::glf().glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8,
	                                       width, height);
	GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// attach it to currently bound framebuffer object
	GLHandler::glf().glFramebufferRenderbuffer(GL_FRAMEBUFFER,
	                                           GL_DEPTH_STENCIL_ATTACHMENT,
	                                           GL_RENDERBUFFER, renderBuffer);
}

void GLFramebufferObject::setName(QString const& name)
{
	this->name    = name;
	auto fullName = "Framebuffer " + QString::number(fbo) + " - " + name;
	GLHandler::glf().glObjectLabel(GL_FRAMEBUFFER, fbo, fullName.size(),
	                               fullName.toLatin1().data());
}

void GLFramebufferObject::bind(GLTexture::CubemapFace face, GLint layer) const
{
	GLHandler::glf().glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	if(fbo != 0 && !isDepthMap)
	{
		if(texColorBuffer->getGLTarget() == GL_TEXTURE_CUBE_MAP)
		{
			GLHandler::glf().glFramebufferTexture2D(
			    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			    static_cast<unsigned int>(face)
			        + GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			    texColorBuffer->getGLTexture(), 0);
		}
		else if(texColorBuffer->getGLTarget() == GL_TEXTURE_3D)
		{
			GLHandler::glf().glFramebufferTexture3D(
			    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			    texColorBuffer->getGLTarget(), texColorBuffer->getGLTexture(),
			    0, layer);
		}
	}
}

void GLFramebufferObject::blitColorBufferToCurrent() const
{
	auto curSize(getCurrentSize());
	// Now 'width' and 'height' hold the dimensions of the color attachment
	blitColorBufferToCurrent(0, 0, width, height, 0, 0, curSize[0], curSize[1]);
}

void GLFramebufferObject::blitColorBufferToCurrent(int srcX0, int srcY0,
                                                   int srcX1, int srcY1,
                                                   int dstX0, int dstY0,
                                                   int dstX1, int dstY1) const
{
	GLHandler::glf().glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
	GLHandler::glf().glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0,
	                                   dstX1, dstY1, GL_COLOR_BUFFER_BIT,
	                                   GL_LINEAR);
}

void GLFramebufferObject::blitColorBufferTo(GLFramebufferObject const& to) const
{
	blitColorBufferTo(to, 0, 0, width, height, 0, 0, to.width, to.height);
}

void GLFramebufferObject::blitColorBufferTo(GLFramebufferObject const& to,
                                            int srcX0, int srcY0, int srcX1,
                                            int srcY1, int dstX0, int dstY0,
                                            int dstX1, int dstY1) const
{
	// Bind the default framebuffer explicitly if it's the destination; NVIDIA
	// is ok not to do that, but not mesa
	if(to.fbo == 0)
	{
		GLHandler::glf().glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}
	GLHandler::glf().glBlitNamedFramebuffer(fbo, to.fbo, srcX0, srcY0, srcX1,
	                                        srcY1, dstX0, dstY0, dstX1, dstY1,
	                                        GL_COLOR_BUFFER_BIT, GL_LINEAR);
}

void GLFramebufferObject::blitDepthBufferTo(GLFramebufferObject const& to) const
{
	// Bind the default framebuffer explicitly if it's the destination; NVIDIA
	// is ok not to do that, but not mesa
	if(to.fbo == 0)
	{
		GLHandler::glf().glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}
	GLHandler::glf().glBlitNamedFramebuffer(fbo, to.fbo, 0, 0, width, height, 0,
	                                        0, to.width, to.height,
	                                        GL_DEPTH_BUFFER_BIT, GL_NEAREST);
}

void GLFramebufferObject::showOnWindow(int x0, int y0, int x1, int y1) const
{
	blitColorBufferTo({}, 0, 0, width, height, x0, y0, x1, y1);
}

void GLFramebufferObject::showOnWindow(QWindow const& window, float xf0,
                                       float yf0, float xf1, float yf1) const
{
	QSize windowSize(window.size());
	windowSize *= window.screen()->devicePixelRatio();
	blitColorBufferTo({}, 0, 0, width, height, windowSize.width() * xf0,
	                  windowSize.height() * yf0, windowSize.width() * xf1,
	                  windowSize.height() * yf1);
}

QImage GLFramebufferObject::copyColorBufferToQImage() const
{
	QImage result(width, height, QImage::Format::Format_RGBA8888);
	GLHandler::glf().glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
	GLHandler::glf().glReadPixels(0, 0, width, height, GL_RGBA,
	                              GL_UNSIGNED_BYTE, result.bits());
	return result;
}

void GLFramebufferObject::cleanUp()
{
	if(!doClean)
	{
		return;
	}
	--instancesCount();
	GLHandler::glf().glDeleteRenderbuffers(1, &renderBuffer);
	GLHandler::glf().glDeleteFramebuffers(1, &fbo);
	doClean = false;
}

std::array<int, 3> GLFramebufferObject::getCurrentSize()
{
	// Get the object name bound to the color attachment
	GLint attachmentObjectName = 0;
	GLHandler::glf().glGetFramebufferAttachmentParameteriv(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &attachmentObjectName);

	// Check if the attachment is a texture or renderbuffer
	GLint attachmentObjectType = 0;
	GLHandler::glf().glGetFramebufferAttachmentParameteriv(
	    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &attachmentObjectType);

	GLint curWidth = -1, curHeight = -1, curDepth = -1;
	if(attachmentObjectType == GL_TEXTURE)
	{
		// TODO(florian) get the correct glTarget!
		// Bind the texture and get its size
		GLHandler::glf().glBindTexture(GL_TEXTURE_2D, attachmentObjectName);
		GLHandler::glf().glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,
		                                          GL_TEXTURE_WIDTH, &curWidth);
		GLHandler::glf().glGetTexLevelParameteriv(
		    GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &curHeight);
		GLHandler::glf().glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,
		                                          GL_TEXTURE_HEIGHT, &curDepth);
	}
	else if(attachmentObjectType == GL_RENDERBUFFER)
	{
		// Bind the renderbuffer and get its size
		GLHandler::glf().glBindRenderbuffer(GL_RENDERBUFFER,
		                                    attachmentObjectName);
		GLHandler::glf().glGetRenderbufferParameteriv(
		    GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &curWidth);
		GLHandler::glf().glGetRenderbufferParameteriv(
		    GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &curHeight);
		curDepth = 1;
	}

	return {curWidth, curHeight, curDepth};
}

void GLFramebufferObject::pushCurrentOnBindStack()
{
	GLint curFboR = 0, curFboW = 0;
	GLHandler::glf().glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &curFboR);
	GLHandler::glf().glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &curFboW);
	bindStack().push_back({curFboR, curFboW});
}

void GLFramebufferObject::applyCurrentFromBindStack()
{
	auto pair = bindStack().back();
	GLHandler::glf().glBindFramebuffer(GL_READ_FRAMEBUFFER, pair.first);
	GLHandler::glf().glBindFramebuffer(GL_DRAW_FRAMEBUFFER, pair.second);
}

void GLFramebufferObject::popCurrentFromBindStack()
{
	applyCurrentFromBindStack();
	bindStack().pop_back();
}
