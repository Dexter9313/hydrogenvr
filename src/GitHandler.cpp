/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "GitHandler.hpp"

#include <QtDebug>

#ifdef GIT
#include <git2/sys/repository.h>
#endif
/*void GitHandler::Repository::Remote::fetch() const
{
#ifdef GIT
    git_remote_fetch(remote, nullptr, nullptr, nullptr);
#endif
}
*/

// not static to make sure git2 was initialized
// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
void GitHandler::clone(QString const& url, QString const& path)
{
#ifdef GIT
	git_repository* repo = nullptr;
	git_clone(&repo, url.toLatin1().data(), path.toLatin1().data(), nullptr);
	git_repository_free(repo);
	repo = nullptr;
#endif
}

// not static to make sure git2 was initialized
// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
GitHandler::LsRemoteOutput GitHandler::lsRemote(QString const& url)
{
#ifdef GIT
	git_repository* repo = nullptr;
	git_remote* remote   = nullptr;
	int error            = git_repository_new(&repo);
	if(error != 0)
	{
		qWarning() << "Git: Failed to create new repository from url " << url
		           << ":" << git_error_last()->message;
		return {};
	}
	error = git_remote_create_anonymous(&remote, repo, url.toLatin1().data());
	if(error != 0)
	{
		qWarning() << "Git: Failed to create remote from url " << url << ":"
		           << git_error_last()->message;
		return {};
	}

	// Connect to the remote
	error = git_remote_connect(remote, GIT_DIRECTION_FETCH, nullptr, nullptr,
	                           nullptr);
	if(error != 0)
	{
		qWarning() << "Git: Failed to connect to remote from url " << url << ":"
		           << git_error_last()->message;
		git_remote_free(remote);
		return {};
	}

	const git_remote_head** refs = nullptr;
	size_t refs_len              = 0;
	error                        = git_remote_ls(&refs, &refs_len, remote);
	if(error != 0)
	{
		qWarning() << "Failed to list remote references from url " << url << ":"
		           << git_error_last()->message;
		git_remote_free(remote);
		return {};
	}

	LsRemoteOutput result;
	for(size_t i = 0; i < refs_len; i++)
	{
		QString name(refs[i]->name);
		if(name.startsWith("refs/heads/"))
		{
			result.branches << name.remove("refs/heads/");
		}
		else if(name.startsWith("refs/tags/") && !name.endsWith("^{}"))
		{
			result.tags << name.remove("refs/tags/");
		}
	}

	git_remote_free(remote);

	return result;
#else
	(void) url;
	return {};
#endif
}

// RAII DEFS

GitHandler::GitHandler()
{
#ifdef GIT
	git_libgit2_init();
#endif
}

GitHandler::~GitHandler()
{
#ifdef GIT
	git_libgit2_shutdown();
#endif
}
/*
GitHandler::Repository::Repository(QString const& path)
{
#ifdef GIT
    git_repository_open(&repo, path.toLatin1().data());
#endif
}

GitHandler::Repository::~Repository()
{
#ifdef GIT
    git_repository_free(repo);
    repo = nullptr;
#endif
}

#ifdef GIT
GitHandler::Repository::Remote::Remote(git_repository& repo,
                                       QString const& name)
{
    git_remote_lookup(&remote, &repo, name.toLatin1().data());
}
#endif

GitHandler::Repository::Remote::~Remote()
{
#ifdef GIT
    git_remote_free(remote);
    remote = nullptr;
#endif
};

#ifdef GIT
GitHandler::Repository::Reference::Reference(git_repository& repo,
                                             QString const& name)
{
    git_reference_lookup(&ref, &repo, name.toLatin1().data());
}
#endif

GitHandler::Repository::Reference::~Reference()
{
#ifdef GIT
    git_reference_free(ref);
    ref = nullptr;
#endif
};
*/
