/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Zip.hpp"

#ifdef QUAZIP
#include <quazip/quazip.h>
#include <quazip/quazipfile.h>

#include <QCoreApplication>
#include <QLabel>
#include <QProgressDialog>
#include <QThread>
#include <QtDebug>

#include <memory>

#endif

bool Zip::isEnabled()
{
#ifdef QUAZIP
	return true;
#else
	return false;
#endif
}

bool Zip::decompress(QString const& zipSourcePath, QDir const& target,
                     bool showProgress, QWidget* parent)
{
#ifdef QUAZIP
	QuaZip zip(zipSourcePath);

	if(!zip.open(QuaZip::mdUnzip))
	{
		qWarning() << "Failed to open ZIP file:" << zipSourcePath;
		return false;
	}

	if(!target.exists() && !target.mkpath("."))
	{
		qWarning() << "Failed to create output directory:" << target.path();
		return false;
	}

	std::unique_ptr<QProgressDialog> progressDialog;
	std::unique_ptr<QLabel> progressLabel;
	bool cancel             = false;
	size_t totalExtractedMB = 0;
	for(auto const& fileInfo : zip.getFileInfoList64())
	{
		totalExtractedMB += fileInfo.uncompressedSize;
	}
	totalExtractedMB /= 1024 * 1024;

	if(showProgress)
	{
		progressLabel = std::make_unique<QLabel>(parent);
		progressLabel->setAlignment(Qt::AlignLeft);

		progressDialog = std::make_unique<QProgressDialog>(parent);
		progressDialog->setMaximum(totalExtractedMB);
		progressDialog->setWindowTitle(QObject::tr("Extracting..."));
		progressDialog->setLabel(progressLabel.get());
		progressDialog->show();

		QObject::connect(progressDialog.get(), &QProgressDialog::canceled,
		                 [&cancel]() { cancel = true; });
	}

	size_t currentlyExtractedBytes = 0;
	for(bool more = zip.goToFirstFile(); more && !cancel;
	    more      = zip.goToNextFile())
	{
		const QString fileName = zip.getCurrentFileName();
		const QString fullPath = target.filePath(fileName);

		if(showProgress)
		{
			progressLabel->setText(fileName);
			progressDialog->setValue(currentlyExtractedBytes / (1024 * 1024));
		}
		QCoreApplication::processEvents();
		QThread::msleep(10);

		if(fileName.endsWith('/'))
		{
			// Directory entry
			if(!target.mkpath(fileName))
			{
				qWarning() << "Failed to create directory:" << fileName;
				return false;
			}
		}
		else
		{
			// File entry
			QFile outputFile(fullPath);
			if(!outputFile.open(QIODevice::WriteOnly))
			{
				qWarning() << "Failed to open file for writing:" << fullPath;
				return false;
			}

			QuaZipFile file(&zip);
			if(!file.open(QIODevice::ReadOnly))
			{
				qWarning() << "QUAZIP ERROR (" << file.getZipError()
				           << "): Failed to open file inside ZIP:" << fileName;
				return false;
			}

			const qint64 chunkSize = 4 * 1024 * 1024;
			// if < 4MB just write
			if(file.usize() < chunkSize)
			{
				outputFile.write(file.readAll());
				currentlyExtractedBytes += file.usize();
			}
			else // else write 4MB by 4MB
			{
				QByteArray buffer(chunkSize, Qt::Uninitialized);
				while(file.bytesAvailable() > 0)
				{
					auto read = file.read(buffer.data(), chunkSize);
					currentlyExtractedBytes += read;
					outputFile.write(buffer, read);

					if(showProgress)
					{
						progressDialog->setValue(currentlyExtractedBytes
						                         / (1024 * 1024));
						QCoreApplication::processEvents();
					}
				}
			}
		}
	}

	if(cancel)
	{
		target.rmpath(target.path());
		return false;
	}

	return true;
#else
	(void) zipSourcePath;
	(void) target;
	(void) showProgress;
	(void) parent;
	return false;
#endif
}
