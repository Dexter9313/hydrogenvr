# Find OPENVR
#
# Sets OPENVR_FOUND, OPENVR_INCLUDE_DIRS, OPENVR_LIBRARIES
#

# pkg-config
find_package(PkgConfig)
if(PkgConfig_FOUND)
	pkg_check_modules(openvr openvr)

	# TODO: remove this hack, keep "openvr"
	set(OpenVR_FOUND "${openvr_FOUND}")
	set(OPENVR_INCLUDE_DIRS "${openvr_INCLUDE_DIRS}")
	set(OPENVR_LIBRARIES "${openvr_LIBRARIES}")
	set(OPENVR_VERSION "${openvr_VERSION}")

	# Use regex to capture major, minor, and build numbers
	string(REGEX MATCH "^([0-9]+)\\.([0-9]+)\\.([0-9]+)$" _ ${openvr_VERSION})

	# Extract captured groups into variables
	set(OPENVR_VERSION_MAJOR "${CMAKE_MATCH_1}")
	set(OPENVR_VERSION_MINOR "${CMAKE_MATCH_2}")
	set(OPENVR_VERSION_BUILD "${CMAKE_MATCH_3}")

endif()
