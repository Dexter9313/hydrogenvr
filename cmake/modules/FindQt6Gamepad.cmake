# Find Qt6Gamepad
#
# Sets Qt6Gamepad_FOUND, QT6GAMEPAD_INCLUDE_DIRS, QT6GAMEPAD_LIBRARIES
#


# Attempt as pkg-config
find_package(PkgConfig)
if(PkgConfig_FOUND)
	pkg_check_modules(Qt6Gamepad Qt6Gamepad)
endif()

# Attempt in /usr/local
if(NOT Qt6Gamepad_FOUND)
	find_path(Qt6Gamepad_INSTALL_DIR QtGamepad/qgamepad.h NAMES "include/qt6/QtGamepad/qgamepad.h" "include/x86_64-linux-gnu/qt6/QtGamepad/qgamepad.h"  PATHS "/usr/local" "/usr")
	find_path(Qt6Gamepad_INCLUDE_DIR QtGamepad/qgamepad.h PATHS  "${Qt6Gamepad_INSTALL_DIR}/include/qt6" "${Qt6Gamepad_INSTALL_DIR}/include/x86_64-linux-gnu/qt6" DOC "Path to the Qt6Gamepad include directory" NO_DEFAULT_PATH)
	find_library(Qt6Gamepad_LIBRARY NAMES Qt6Gamepad PATHS "${Qt6Gamepad_INSTALL_DIR}/lib" "${Qt6Gamepad_INSTALL_DIR}/lib/x86_64-linux-gnu" DOC "The Qt6Gamepad library.")
	find_library(Qt6UniversalInput_LIBRARY NAMES Qt6UniversalInput PATHS "${Qt6Gamepad_INSTALL_DIR}/lib" "${Qt6Gamepad_INSTALL_DIR}/lib/x86_64-linux-gnu" DOC "The Qt6UniversalInput library.")

	set(Qt6Gamepad_FOUND 0)
	if(Qt6Gamepad_INCLUDE_DIR AND Qt6Gamepad_LIBRARY)
		set(Qt6Gamepad_FOUND 1)
		set(Qt6Gamepad_INCLUDE_DIRS ${Qt6Gamepad_INCLUDE_DIR})
		if(NOT Qt6UniversalInput_LIBRARY)
			message(WARNING "Qt6Gamepad was found, but Qt6UniversalInput was not found. This can be ignored depending on the Qt6Gamepad version.")
			set(Qt6Gamepad_LIBRARIES ${Qt6Gamepad_LIBRARY})
		else()
			set(Qt6Gamepad_LIBRARIES ${Qt6Gamepad_LIBRARY} ${Qt6UniversalInput_LIBRARY})
		endif()
	endif()
endif()

# Attempt as Qt6 official CMake package
if(NOT Qt6Gamepad_FOUND)
	find_package(Qt6 OPTIONAL_COMPONENTS Gamepad)
	if(Qt6Gamepad_FOUND)
		set(Qt6Gamepad_LIBRARIES Qt6::Gamepad)
	endif()
endif()

