# taken from: https://github.com/facebook/folly/blob/master/CMake/FindZstd.cmake
# SPDX-FileCopyrightText: Facebook, Inc. and its affiliates.
# SPDX-License-Identifier: Apache-2.0
#
# - Try to find Facebook zstd library
# This will define
# libzstd_FOUND
# libzstd_INCLUDE_DIRS
# libzstd_LIBRARIES
#

# pkg-config
find_package(PkgConfig)
if(PkgConfig_FOUND)
	pkg_check_modules(libzstd libzstd)
endif()
