# Find libgit2
#
# Sets libgit2_FOUND, libgit2_INCLUDE_DIRS, libgit2_LIBRARIES
#

# pkg-config
find_package(PkgConfig)
if(PkgConfig_FOUND)
	pkg_check_modules(quazip1-qt6 quazip1-qt6)
endif()

if(NOT quazip1-qt6_FOUND)
	find_path(quazip1-qt6_INSTALL_DIR quazip/quazip.h NAMES "include/quazip/quazip.h" "include/QuaZip-Qt6-1.4/quazip/quazip.h"  PATHS "/usr/local" "/usr")
	find_path(quazip1-qt6_INCLUDE_DIR quazip/quazip.h PATHS  "${quazip1-qt6_INSTALL_DIR}/include" "${quazip1-qt6_INSTALL_DIR}/include/QuaZip-Qt6-1.4" DOC "Path to the quazip1-qt6 include directory" NO_DEFAULT_PATH)
	find_library(quazip1-qt6_LIBRARY NAMES quazip1-qt6 PATHS "${quazip1-qt6_INSTALL_DIR}/lib" DOC "The quazip1-qt6 library.")

	set(quazip1-qt6_FOUND 0)
	if(quazip1-qt6_INCLUDE_DIR AND quazip1-qt6_LIBRARY)
		set(quazip1-qt6_FOUND 1)
		set(quazip1-qt6_MANUAL_BUILD 1)
		set(quazip1-qt6_INCLUDE_DIRS ${quazip1-qt6_INCLUDE_DIR})
		set(quazip1-qt6_LIBRARIES ${quazip1-qt6_LIBRARY})
	endif()
endif()
