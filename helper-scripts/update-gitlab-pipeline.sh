#!/bin/bash

cat ci/gitlab-ci/build-conf/*.yml > ci/gitlab-ci/main.yml

cat ci/gitlab-ci/archlinux/install_dependencies.sh ci/gitlab-ci/archlinux/Dockerfile ci/gitlab-ci/build-conf/archlinux.yml | sha256sum > .pipeline-hashes/archlinux
cat ci/gitlab-ci/ubuntu/install_dependencies.sh ci/gitlab-ci/ubuntu/22.04/Dockerfile ci/gitlab-ci/build-conf/ubuntu-22.04.yml | sha256sum > .pipeline-hashes/ubuntu-22.04
cat ci/gitlab-ci/ubuntu/install_dependencies.sh ci/gitlab-ci/ubuntu/24.04/Dockerfile ci/gitlab-ci/build-conf/ubuntu-24.04.yml | sha256sum > .pipeline-hashes/ubuntu-24.04
