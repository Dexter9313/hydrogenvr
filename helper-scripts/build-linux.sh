#!/bin/bash -e

if [ -f ./project_directory.conf ]; then
    . ./project_directory.conf
else
    PROJECT_DIRECTORY="example"
    HVR_DIRECTORY="."
fi
. ./${PROJECT_DIRECTORY}/build.conf

if [[ -z "$CMAKE_BUILD_TYPE" ]];
then
	CMAKE_BUILD_TYPE=Release
fi

BUILD_DIR=build/
UP_DIR=".."
if [[ "$CMAKE_BUILD_TYPE" != "Release" ]]
then
	BUILD_DIR=build/Debug
	UP_DIR="../.."
fi

mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake $UP_DIR -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE

if [[ $# -ne 1 ]]
then
	ncpus=$(lscpu --parse -a | grep -v "^#" | wc -l)
else
	ncpus="$@"
fi

echo "Check GLSL code"
make validate-glsl
echo "make -j $ncpus"
make -j $ncpus
