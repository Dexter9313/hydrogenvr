#version 420 core
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_image_load_store : enable
#extension GL_ARB_shader_image_size : enable

layout(local_size_x = 4, local_size_y = 4,
       local_size_z = 2) in;

uniform samplerCube environmentMap;
layout(rgba32f, binding = 1) uniform writeonly imageCube dataOut;

uniform float roughness = 0.0;

const float PI = 3.14159265359;

float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 Hammersley(uint i, uint N)
{
	return vec2(float(i) / float(N), RadicalInverse_VdC(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness)
{
	float a = roughness * roughness;

	float phi      = 2.0 * PI * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a * a - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	// from spherical coordinates to cartesian coordinates
	vec3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;

	// from tangent-space vector to world-space sample vector
	vec3 up      = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangent = normalize(cross(up, N));
	vec3 bitangent = cross(N, tangent);

	vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
	return normalize(sampleVec);
}

vec3 computeDirectionFromTexel(ivec3 texelCoords, int cubemapSize)
{
	// Determine which face of the cubemap the texel is on
	int face = texelCoords.z;

	// Normalize texel coordinates to [-1, 1]
	vec2 coord = (vec2(texelCoords.xy) / float(cubemapSize - 1)) * 2.0 - 1.0;

	// Compute direction based on the face
	vec3 direction;
	switch(face)
	{
		case 0:
			direction = vec3(1.0, -coord.y, -coord.x);
			break; // Positive X
		case 1:
			direction = vec3(-1.0, -coord.y, coord.x);
			break; // Negative X
		case 2:
			direction = vec3(coord.x, 1.0, coord.y);
			break; // Positive Y
		case 3:
			direction = vec3(coord.x, -1.0, -coord.y);
			break; // Negative Y
		case 4:
			direction = vec3(coord.x, -coord.y, 1.0);
			break; // Positive Z
		case 5:
			direction = vec3(-coord.x, -coord.y, -1.0);
			break; // Negative Z
		default:
			break;
	}

	return normalize(direction);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
	float a      = roughness * roughness;
	float a2     = a * a;
	float NdotH  = max(dot(N, H), 0.0);
	float NdotH2 = NdotH * NdotH;

	float num   = a2;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	denom       = PI * denom * denom;

	return num / denom;
}

float computeMipLevel(vec3 N, vec3 H, float HdotV, uint SAMPLE_COUNT)
{
	const float resolution = float(textureSize(environmentMap, 0).x);

	float D     = DistributionGGX(N, H, roughness);
	float NdotH = dot(N, H);
	float pdf   = (D * NdotH / (4.0 * HdotV)) + 0.0001;

	float saTexel  = 4.0 * PI / (6.0 * resolution * resolution);
	float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);

	float mipLevel = roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);
	return mipLevel + 1.0;
}

void main()
{
	ivec3 pixel_coords = ivec3(gl_GlobalInvocationID.xyz);

	vec3 N = computeDirectionFromTexel(pixel_coords, imageSize(dataOut).x);
	vec3 R = N;
	vec3 V = R;

	const uint SAMPLE_COUNT = 24u + uint(1000 * roughness);
	float totalWeight       = 0.0;
	vec3 prefilteredColor   = vec3(0.0);
	for(uint i = 0u; i < SAMPLE_COUNT; ++i)
	{
		vec2 Xi = Hammersley(i, SAMPLE_COUNT);
		vec3 H  = ImportanceSampleGGX(Xi, N, roughness);
		vec3 L  = normalize(2.0 * dot(V, H) * H - V);

		float NdotL = max(dot(N, L), 0.0);
		if(NdotL > 0.0)
		{
			float mipLevel = computeMipLevel(N, H, dot(H, V), SAMPLE_COUNT);
			prefilteredColor
			    += textureLod(environmentMap, L, mipLevel).rgb * NdotL;
			totalWeight += NdotL;
		}
	}
	prefilteredColor = prefilteredColor / totalWeight;

	imageStore(dataOut, pixel_coords, vec4(prefilteredColor, 0.0));
}
